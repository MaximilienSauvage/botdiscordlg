//Lecture de fichier
var fs = require('fs');
//Connexion à l'API de Discord
const Discord  = require('discord.js')
const bot = new Discord.Client()
const lgCommand = require('./Function/LG/Commands/allCommand.js')

//Log grâce au token utilisateur
bot.login("")


//Variables d'etat du jeu
var states = {}
states["isInGame"]    = false
states["beginned"]    = false
states["random"] 	  = false;
states["hiddenCompo"] = false;
states["timedGame"]   = true;
states["vocalOn"]     = false;
states["creator"]     = null;
states["compo"]       = [];
states["players"]     = [];
states["attribution"] = [];
states["allPossibleRoles"] = [];
states["villageVote"] = {};
states["LgVote"] = {};
states["heure"] = "Nuit";
states["tempsPhase"] = 120000

bot.on('ready',function(){
	var contents = fs.readFileSync('./Assets/roles.txt', 'utf8');
	var testSplit = contents.split("\r\n")

	for (var i =0 ; i< testSplit.length; i++)
	{
		states["allPossibleRoles"][i] = require('./Function/LG/Role/' + testSplit[i] + '.js')
	}
})

bot.on('message',function(msg){
	
	lgCommand.checkCommands(msg,states)

})