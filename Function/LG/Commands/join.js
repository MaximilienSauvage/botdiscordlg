const lgGame = require('./lgGame.js')

module.exports = class join extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "join" ) return true
		return false
	}

	static action(message,states){

		var commander = message.member
		var chan = message.channel
		
		console.log(commander.displayName + " tente de rejoindre")
		message.delete()
		if(states["isInGame"] && !states["beginned"]){
			
			var alreadyHere = false
			for (var i =0 ; i< states["players"].length; i++)
			{
				if(states["players"][i] == commander){
					alreadyHere = true	
					break;
				} 
			}
			if(!alreadyHere)
			{
				chan.send(commander.displayName + " a rejoint la partie")
				states["players"].push(commander)
			}
		}
		//console.log(states)
	}
} 