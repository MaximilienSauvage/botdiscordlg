const lgGame = require('./lgGame.js')

module.exports = class killGame extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "killGame" ) return true
		return false
	}

	static action(message,states){

		var commander = message.member.user

		if (commander == states["creator"] || true){
			message.member.send("Partie terminée par " + states["creator"])
			
			states["isInGame"]    = false
			states["beginned"]    = false
			states["random"] 	  = false;
			states["hiddenCompo"] = false;
			states["timedGame"]   = true;
			states["vocalOn"]     = false;
			states["creator"]     = null;
			states["compos"]      = [];
			states["players"]     = [];
			states["roles"]       = [];
		
			//Retirer les roles de villager dead villager et permission loup-garou
			//Clean les chans Village, LG et cimetierre
			//Reset les booleens de pouvoir
		}
	}
} 