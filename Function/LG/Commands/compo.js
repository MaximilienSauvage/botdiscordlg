const lgGame = require("./lgGame.js")

module.exports = class compo extends lgGame{
	
	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "compo" ) return true
		return false
	}

	static action(message,states)
	{
		if(!states["hiddenCompo"]){
			let chan = message.channel
			let args = message.content.split(" ")
			message.delete()
			if (args.length <= 2)
			{
				var compoReturned = "```\n\n";
				if(states["compo"].length >0){
					for (var i =0 ; i< states["compo"].length; i++)
					{
						compoReturned += "\n-" + states["compo"][i]
					}
					compoReturned += "\n```"
					chan.send(compoReturned)
				}else{
					chan.send("La composition est vide pour l'instant.\n Le createur de la partie peut la completer grâce à : *!lg compo add*")
				}
				
			}else{
				
				if(states["creator"] == message.member.user){
					
					//Ajouter un ou des elements à la composition
					if(args[2] == "add")
					{
						if(args.length >3){
							var allAdded = "Role(s) ajouté(s) : \n"
							for (var i =3 ; i<args.length; i++)
							{
								var currentAdd = args[i]
								for(var j =0; j<states["allPossibleRoles"].length ;j++){

									if(states["allPossibleRoles"][j].name() == currentAdd)
									{
										states["compo"].push(currentAdd)
										allAdded += currentAdd + "\n"
										break
									}
								}
							}
							console.log(allAdded)
							chan.send(allAdded)
						}
					}
					
					//Supprimer un ou des elements de la composition
					if(args[2] == "del")
					{

						if(args.length >3){
							var allDel = "Rôle(s) supprimé(s) : \n "
							for (var i =3 ; i<args.length; i++)
							{

								for(var j =0; j<states["allPossibleRoles"].length ;j++){

									if(states["allPossibleRoles"][j].name() == currentDel)
									{
										states["compo"].remove(currentDel)
										allDel += (currentDel + " retiré(e) de la partie \n ")
										break
									}
								}
							}
							console.log(allDel)
							chan.send(allDel)
						}
					}
					
					//Vider completement la compo
					if(args[2] == "clear")
					{
						console.log("Compo cleared")
						states["compo"] = []
						chan.send("Composition remise à zéro")
					}
					
					//Ajout du préset initial du loup garou
					if(args[2] == "preset")
					{
						console.log("preset pasted")
						chan.send("Application d'un preset standard")
						states["compo"] = []
						states["compo"] = ["SV","SV","LG","LG","Voyante","Sorciere","Cupidon","Chasseur"]
						if(args.length >3){
							if(args[3] == "11"){
								states["compo"].push("PF")
								states["compo"].push("Ancien")
								states["compo"].push("SV")
							}
						}
					}
				}
			}
		}
		//sortCompo(states)
	}
}

function sortCompo(states)
{
	states["compos"].sort(function (a,b) {

		estLG_a = a.startsWith("LG")
		estLG_b = b.startsWith("LG")
		
		if(estLG_a && !estLG_b) return 1
		else if(estLG_b && !estLG_b) return -1
		else if((estLG_b && estLG_a) ||(!estLG_b && !estLG_a)) return 0 
	})
}