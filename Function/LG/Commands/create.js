const lgGame = require('./lgGame.js')

module.exports = class create extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "create" ) return true
		return false
	}

	static action(message,states){
		
		var commander = message.member
		var chan = message.channel
		message.delete()
		console.log(" Creation de la partie par "+ commander.displayName + " ... ")
		if(!states["isInGame"]){
			chan.send("@everyone \n Creation d'une nouvelle partie par : " + commander.displayName + "\n !lg join pour rejoindre la partie")
			states["isInGame"] = true
			states["creator"]  = commander.user
			states["players"]  = []
					
		}else{
			commander.send("Partie déjà en cours. Attendez la fin de celle-ci")
		}
	}
} 

