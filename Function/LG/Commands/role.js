const lgGame = require('./lgGame.js')

module.exports = class role extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "role" || args[1]=="vote" ) return true
		return false
	}
	
	static action(message,states){
		
		var commander = message.member
		var guild = message.guild
		var chan = message.channel
		let args = message.content.split(" ")

		if (args.length >2 ){
			if(chan.name == "village" || chan.name == "loup-garou" )
			{
				var a = states["players"].indexOf(commander)
				
				if(a!=-1) var myRole = states["compo"][a]
				for(var i =0; i<states["allPossibleRoles"].length ;i++){
					if(myRole == states["allPossibleRoles"][i].name())
					{
						
						if (args[2]>-1 && args[2]<states["players"].length){
							if (args[1] == "role"){
								states["allPossibleRoles"][i].activePower(message,states)
							}else if (args[1] == "vote"){
								states["allPossibleRoles"][i].villageVote(message,states)
							}
						}
						break;
					}
				}
			}
		}
		message.delete()
	}

}