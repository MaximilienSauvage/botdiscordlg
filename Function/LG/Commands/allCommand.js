const creation = require('./create.js')
const join = require('./join.js')
const kill = require('./killGame.js')
const nbPlayer = require('./nb.js')
const begin = require('./begin.js')
const help = require('./help.js')
const compo = require('./compo.js')
const role = require('./role.js')
const clearChan = require('./clearChannel.js')

module.exports = class allCommand {

	static checkCommands(message,states)
	{
		let a = creation.parse(message,states) 
				|| join.parse(message,states) 
				|| kill.parse(message,states) 
				|| nbPlayer.parse(message,states) 
				|| begin.parse(message,states) 
				|| help.parse(message,states)
				|| compo.parse(message,states)
				|| role.parse(message,states)
				|| clearChan.parse(message,states)
	}

}