const lgGame = require('./lgGame.js')

module.exports = class begin extends lgGame{


	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "begin" ) return true
		return false
	}

	static action(message,states){
		

		var commander = message.member
		var chan  = message.channel
		var guild = message.guild
		let roleVillager = guild.roles.find(r => r.name === "Villager");
		let roleDead = guild.roles.find(r => r.name === "Dead Villager");
		//console.log(roleVillager)
		//console.log(roleDead)

		if (commander.user == states["creator"] && !states["beginned"] && states["isInGame"]){
			console.log(states["compo"].length +">=" + states["players"].length)
			if(states["compo"].length >= states["players"].length){
				

				states["beginned"] = true;
				/////////////////////////////////////////////////////////////////////
				//           Attribution des rôles et log dans la console          //
				/////////////////////////////////////////////////////////////////////

				states["players"]=shuffle(states["players"]);
				states["compo"]=shuffle(states["compo"]);
				chan.send("La partie va commencer. \n Attribution des rôles en privée ...")
				
				for (var i =0 ; i< states["players"].length; i++)
				{
					console.log(i + " : " + states["players"][i].user + " est " + states["compo"][i])
				}
				
				/////////////////////////////////////////////////////////////////////
				//   MP les joueurs leurs rôle en utilisant l'object description   //
				/////////////////////////////////////////////////////////////////////
				//
				for (var i =0 ; i< states["players"].length; i++)
				{
					var currentPlayer = states["players"][i]
					var currentPlayerRole = states["compo"][i]
					//console.log(currentPlayer)
					//console.log(currentPlayerRole)

					for(var j =0; j<states["allPossibleRoles"].length ;j++){

						if(currentPlayerRole == states["allPossibleRoles"][j].name()){
							states["allPossibleRoles"][j].description(states["players"][i])
							break
						}
					}
				}
				/////////////////////////////////////////////////////////////////////
				//Donner aux joueurs le role de villager pour ecrire dans le chan. //
				/////////////////////////////////////////////////////////////////////
				for (var i =0 ; i< states["players"].length; i++)
				{
					states["players"][i].addRole(roleVillager)
					var LGchan = message.guild.channels.find(channel => channel.name === "loup-garou");
					if (states["compo"][i].startsWith("LG")){
						var tmp = 	{
									 'SEND_MESSAGES': true,
									 'READ_MESSAGES': true
									}
						LGchan.overwritePermissions(states["players"][i].user,tmp)
					}else{
						var tmp = 	{
									 'SEND_MESSAGES': false,
									 'READ_MESSAGES': false
									}
						LGchan.overwritePermissions(states["players"][i].user,tmp)
					}
					
				}
				states["beginned"] = true;
			
			


				// Afficher l'agencement du village
				var playerList=" Voici l'agencement du village : \n"
				for (var i =0 ; i< states["players"].length; i++)
				{
					playerList += i + " - " + states["players"][i].displayName + "\n"
				}
				var villageChan = chan.guild.channels.find(r => r.name === "village");

				
				villageChan.send("Après ces quelques présentations, le village s'endort épuisé de sa journée ...")
				setInterval(changerTemps(chan,states),states["tempsPhase"])
				villageChan.send("```" + playerList +"```")
			}
		}else chan.send("Il n'y a pas assez de rôle pour commencer la partie")
		message.delete()
	}	
} 

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function changerTemps(chan,states)
{
	let chanVillage = chan.guild.channels.find(r => r.name === "village");
	return function(){
		
		if(states["heure"] == "Nuit"){
			//demute les gens mute le chan LG
			console.log(states["LgVote"])
			states["heure"] = "Jour"
			chanVillage.send("Le village se reveille ...\n Vous pouvez voter pour éliminer quelqu'un à l'aide de : *!lg vote [NumEmplacement]* ")
			states["LgVote"] = []
			

		}else if(states["heure"] == "Jour"){
			
			resolveVote(chan,states)
			//mute les gens demute les LG
			console.log(states["villageVote"])
			states["heure"] = "Nuit"


			chanVillage.send("Le village s'endort ... ")
			states["villageVote"] = []
		}
		console.log("Phase de "+ states["heure"])
	}
}

function resolveVote(chan,states)
{
	var resultat = ""
	var counting = []
	let chanVillage = chan.guild.channels.find(r => r.name === "village");
	
	for( var i = 0; i<states["players"].length;i++)
	{
		count = 0
		for(var j = 0;j<states["villageVote"].length;j++){
			if(states["villageVote"][j].displayName == states["players"][i].displayName)count++
		}
		console.log(count)
		counting[i]=count
		voter = states["players"].displayName
		console.log(voter)
		resultat += voter +" --->" + states["villageVote"][voter] + "\n"
	}
	//Action pré-revelation
	chanVillage.send(resultat)
	var maxCount = Math.max(counting)
	if(maxCount > 0){
		var killed = counting.indexOf(maxCount)
		chanVillage.send("Le village a voté  :  \n")
		chanVillage.send("Le village a décidé de pendre haut et court " + states["players"][killed].displayName)
	}else chanVillage.send("Le village ne s'est pas décidé, personne ne meurt")
	

	//Tuer le monsieur et donner ou non son role
	//On death event /!\ chasseur oui je te vois

}