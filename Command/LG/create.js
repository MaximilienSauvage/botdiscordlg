const lgGame = require('./lgGame.js')

module.exports = class create extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "create" ) return true
		return false
	}

	static action(message,states){
		
		var commander = message.member
		console.log("@everyone Creation de la partie par "+ commander.displayName + " ... ")
		if(!states["isInGame"]){
			message.channel.send("Creation d'une nouvelle partie par : " + commander.displayName + "\n !lg join pour rejoindre la partie")
			states["isInGame"] = true
			states["creator"]  = commander.user
			states["players"]  = []
					
		}
	}
} 

