module.exports = class lgGame {

	static matchScope(message){
		return message.content.startsWith("!lg")
	}

	static match(message){
		return false
	}

	static action(message){

	}

	static parse(message,states){
		if(this.matchScope(message)){
			if(this.match(message)){
				this.action(message,states)
				return true	
			} 
		}
		return false
	}
} 