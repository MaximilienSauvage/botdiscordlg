const lgGame = require("./lgGame.js")

module.exports = class compo extends lgGame{
	
	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "compo" ) return true
		return false
	}

	static action(message,states)
	{
		if(!states["hiddenCompo"]){
			let args = message.content.split(" ")
			if (args.length <= 2)
			{
				var compoReturned = "";
				for (var i =0 ; i< states["compo"].length; i++)
				{
					compoReturned += "\n-" + states["compo"][i]
				}
				message.channel.send(compoReturned)
			}else{
				if(states["creator"] == message.member.user){
					if(args[2] == "add")
					{
						if(args.length >3){
							states["compo"].push(args[3])
							console.log(args[3] + " added")
							message.channel.send(args[3] + " ajouté(e) à la partie")
						}
					}
					if(args[2] == "del")
					{

						if(args.length >3){
							states["compo"].splice(states["compo"].indexOf(args[3]),1)
							console.log(args[3] + " deleted")
							message.channel.send(args[3] + " retiré(e) de la partie")
						}
					}
					if(args[2] == "clear")
					{
						console.log("Compo cleared")
						states["compo"] = []
						message.channel.send("Composition remise à zéro")
					}
					if(args[2] == "preset")
					{
						console.log("preset pasted")
						states["compo"] = []
						states["compo"] = ["SV","SV","LG","LG","Voyante","Sorciere","Cupidon","Chasseur"]
						if(args.length >3){
							if(args[3] == "11"){
								states["compo"].push("PF")
								states["compo"].push("Ancien")
								states["compo"].push("SV")
							}
						}
					}
				}
			}
		}
	}
}