const lgGame = require('./lgGame.js')

module.exports = class join extends lgGame{

	static match(message){
		let args = message.content.split(" ")
		if (args.length <= 1 ) return false
		if(args[1] == "join" ) return true
		return false
	}

	static action(message,states){

		var commander = message.member
		console.log(commander.displayName + " tente de rejoindre")
		if(states["isInGame"] && !states["beginned"]){
			
			var alreadyHere = false
			for (var i =0 ; i< states["players"].length; i++)
			{
				if(states["players"][i] == commander.user){
					alreadyHere = true	
					break;
				} 
			}
			if(!alreadyHere)
			{
				message.channel.send(commander.displayName + " a rejoint la partie")
				states["players"].push(commander.user)
			}
		}
		console.log(states)
	}
} 